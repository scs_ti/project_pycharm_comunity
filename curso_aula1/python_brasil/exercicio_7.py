##Faça um Programa que calcule a área de um quadrado, em seguida mostre o dobro desta área para o usuário.

v_base = input('Insira o valor da base do quadrado: ')
v_altura = input('Insira o valor da altura do quadrado: ')

r_area = ((int(v_base) * int(v_altura)) * 2)

print('O valor da area do quadrado em dobro é: ' + str(r_area))