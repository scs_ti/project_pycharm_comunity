##Faça um Programa que peça 2 números inteiros e um número real. Calcule e mostre:
##o produto do dobro do primeiro com metade do segundo .
##a soma do triplo do primeiro com o terceiro.
##o terceiro elevado ao cubo.


contador = 1

v_int = []
v_real = []

while (contador < 3):
       n_int = input('Insira o ' + str(contador) + ' valor inteiro:')
       v_int.append(int(n_int))
       contador   = contador + 1

v_real = input('Insira um valor Real: ')

print('Valores inteiros: ' + str(v_int))
print('Valor Real: ' + str(v_real))

r_result = ((2 * int(v_int[0])) * (int(v_int[1]) / 2))

print('O produto do dobro do primeiro com metade do segundo: ' + str(r_result))

r_result1 = ((3 * int(v_int[0])) + int(v_real))

print('A soma do triplo do primeiro com o terceiro: ' + str(r_result1))

r_result3 = int(v_real) ** 3

print('O terceiro elevado ao cubo: ' + str(r_result3))