##trabalhando com copias de listagem

alimentos = ['BATATA', 'OVOS', 'UVAS', 'BANANA', 'PERAS', 'PÃO']

alimentos2 = alimentos[:] ## quando se utiliza o conceito de copia, podemos informar o inicio e fim da listagem,
                        ## sendo assim ele ira compreender que tudo que estiver dentro deste intervalo sera copiado
                        ##para nova variavel

alimentos.append('HAMBURGUERS') ## para confirmação podemos dar um append (inserir elemento) dentro da listagem
                                ## para diferenciar entre a listagem 1 e listagem 2

print('Variavel Alimentos')
print(alimentos)
print('----')
print('Variavel Alimentos2')
print(alimentos2)

## adicionando laco de repeticao para as duas listagem

print('Listagem 1')
for var in alimentos[:]:
    print(var)
    print('------')
print('Fim da listagem')

print('Listagem 2')
for var2 in alimentos2[:]:
    print(var2)
    print('------')
print('FIm da listagem 2')