##trabalhando com fatiamento de listagem dentro de uma variavel e seus elementos

alimentos = ['BATATA', 'OVOS', 'UVAS', 'BANANA', 'PERAS', 'PÃO']

print(alimentos[2:4]) ## colocando dentro dos colchetes o valor inicial (posicao) : ultimo item a ser exibido, ele va
                        ## demonstrar o penultimo item dentro do inserido

print(alimentos[3:]) ## caso nao preencha nada depois dos : sera identificado como totalizador de tudo que à depois do
                      ##valor inserido

print(alimentos[:3]) ## caso nao preencha nada antes dos : sera identificado como totalizador ate o valor final indicado

print(alimentos[::2]) ##caso nao preencha nada entre o valor inicial e final e inserindo o terceiro parametro, ele ira
                        ##reconhecer como valor de intervalo

print(alimentos[-3:]) ## significa que estou buscando a informação do lado direito para esquerda, nao precisando indicar
                        ## o valor final dos elementos

## trabalhando com fatiamento de elementos com laço de repeticao

print('Inicio do laço')

for var in alimentos[:3]:
    print(var)
    print('-----')

print('Fim do Laço')