##trabalhando com listagem em python onde os elementos sao passados dentro de chaves na variavel
##percebe-se que a posição -1 dentro da exibicao da variavel corresponde ao ultimo elemento da listagem
##percebe-se que o inicio da contagem dos elementos sao indicados pelo 0
##manipulacao de elementos dentro de uma lista
carros = ['ford ka', 'civic', 'fusca']
print(carros)

carros [1] = 'mustang'
print(carros)

carros.append('BMW') ##funcao append realiza a adicao do elemento a ultima posicao da listagem
print(carros)

carros.insert(1,'jeep') ##funcao insert realiza a adicao do elemento na posicao inserida como paremetro
print(carros)

carros.extend('gol') ##funcao extend, realiza a adicao do elemento identificando cada caracter em uma posicao da
                        ##listagem
print(carros)


del carros [1]  ##funcao del realiza o delete do elemento dentro da listagem atravez da posicao colocada dentro da
                ##variavel
print(carros)

carros.pop(1) ##funcao pop realiza o delete atravez tambem da posicao informada como paramento
print(carros)

carros.remove('fusca') ##funcao remove, realiza o delete atravez da string da listagem existente
print(carros)
