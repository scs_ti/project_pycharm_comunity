##trabalhando com strings e concatenação
##realizando a criação das variasves e o recebimento das strings, foi concatenado em uma variavel só e printando.

nome1 = 'rafael'
nome2 = 'oliveira'
nome3 = 'inacio'

num = 5

nomecompleto = nome1 + ' ' + nome2 + ' ' + nome3 ##/concatenacao das variaveis

nomecompleto2 = nomecompleto.title()

print(nomecompleto.title()) ##adicioando a funcao title() onde é adicionado as letras maisculas nas strings

print(nomecompleto) ##exibicao padrão

print(nomecompleto2) ##passando a funcao title() direto nas variaveis

print(nome1 + ' ' + nome2 + ' ' + nome3) ##concatenagem dentro da propria exibicao

print('Ola, ' + nomecompleto + ' como vai!') ##passando variavel mais texto fixo

print('Olá %s' %(nomecompleto)) ##passando variavel atravez do contexto % e variavel fora do texto padrao

print('Olá %s' %(nomecompleto) + ' ' + str(num))