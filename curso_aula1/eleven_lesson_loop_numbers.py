##trabalhando com listagem numericas e incluido a função de loop

numeros = [0,1,2,3,4,5,6,7,8,9,10]

print(numeros)

## facilitando a listagem
##funcao list + range pode se usar para listar um inicio e fim lado a lado
numeros2 = list(range(0,100))

print(numeros2)

## laco de repeticao
## com o laco de repeticao, podemos utilizar o conceito de linha a linha
for cont in numeros2:
    print(cont)

## podemos trabalhar com a funcao list + range + intervalos de numeros

numeros3 = list(range(1,10,2)) ## terceito parametro dentro do range significa o intervalo de numeros entre o inicio
                                ## e fim do range
print(numeros3)

## utilizando a funcao range dentro do for tambem pode ser uma possibilidade de inclusao de listagem de numeros

for cont in range(1,10):
    print(cont)

## podemos trabalhar tambem com a inclusao de itens utilizando o append relativo ao processo do loop

listagem = []

for cont in range(1,10):
    listagem.append(cont)

print(listagem)
