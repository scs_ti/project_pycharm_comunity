##trabalhando com organização da listagem
##utilizando o conceito de listagem em colchetes, passado os valores dentro de uma variavel

carros = ['fusca','gol','jeep','ferrari']

print(carros)

## realizando a inclusao de mais um item dentro da listagem

carros.insert(1,'civic')

print(carros)

## funcao .sort() realiza a organização da lista em ordem alfabetica, passando o conteudo (reverse=true), ordem
## da direita para esquerda.

carros.sort()

print(carros)

carros.sort(reverse=True)

print(carros)

## passando a funcao sorted direto na exibição

print(sorted(carros))

## funcao len realiza a contagem de quantidade de elementos dentro da lista

print(len(carros))

## funcao reverse, ordem inversa

carros.reverse()
print(carros)